
# README #

Estes documento README tem como objetivo fornecer as informações para a finalização do teste da Ioasys.

### COMO RODAR ? ###

* Apenas abra o projeto no Android Studio, conecte um dispositivo ou use um emulador e clique no botão Run.

### Bibliotecas usadas ###

* Retrofit: Facilitar comunicações HTTP com a API
* Gson: Facilitar serialização dos dados JSON vindos da API
* Glide: Facilitar o gerenciamento de cache e de memória na exibição das imagens

### O que eu faria a mais ###

* Finalizaria os requisitos do teste
* Tentaria corrigir bugs como o dos erros na tela de login
* Implementaria um padrão de projeto como MVVM

