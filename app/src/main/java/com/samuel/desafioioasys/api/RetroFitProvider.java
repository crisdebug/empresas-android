package com.samuel.desafioioasys.api;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroFitProvider {
    public static final String BASE_URL = "https://empresas.ioasys.com.br/api/v1/";

    private static Retrofit instance;

    public static Retrofit getRetrofitClient() {
        if (instance == null) instance = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();;

        return instance;
    }

    public static String getImageUrl(String imageUri) {
        String newUri = imageUri.substring(1);
        return BASE_URL + newUri;
    }
}
