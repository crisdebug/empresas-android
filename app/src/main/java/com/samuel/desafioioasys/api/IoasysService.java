package com.samuel.desafioioasys.api;

import com.samuel.desafioioasys.model.Credentials;
import com.samuel.desafioioasys.model.EmpresaResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface IoasysService {
    @POST("users/auth/sign_in")
    Call<Void> signIn(@Body Credentials credentials);

    @GET("enterprises")
    Call<EmpresaResponse> listEmpresas(@Query("enterprise_types") int type, @Query("name") String name);

    @GET("enterprises")
    Call<EmpresaResponse> listEmpresasByName(@Query("name") String name, @Header("uid") String uid, @Header("client") String client, @Header("access-token") String accessToken);
}
