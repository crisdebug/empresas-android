package com.samuel.desafioioasys.api;

import android.content.Context;
import android.content.SharedPreferences;

import com.samuel.desafioioasys.model.Credentials;

import retrofit2.Call;
import retrofit2.Retrofit;

import com.samuel.desafioioasys.model.EmpresaResponse;
import com.samuel.desafioioasys.util.Constants;

public class IoasysRepository {
    private Retrofit retrofitClient;
    private IoasysService service;
    private SharedPreferences sharedPreferences;

    public IoasysRepository(Context context) {
        this.retrofitClient = RetroFitProvider.getRetrofitClient();
        this.service = this.retrofitClient.create(IoasysService.class);
        this.sharedPreferences = context.getSharedPreferences(Constants.SHARED_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public Call<Void> getSignInCall(Credentials credentials) {
        return service.signIn(credentials);
    }

    public Call<EmpresaResponse> getEmpresasCall(String nomeEmpresa) {
        return service.listEmpresasByName(nomeEmpresa, this.getUid(), this.getClient(), this.getAccessToken());
    }

    public void saveHeaders(String accessToken, String client, String uid) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("access-token", accessToken);
        editor.putString("client", client);
        editor.putString("uid", uid);
        editor.apply();
    }

    private String getAccessToken() {
        return sharedPreferences.getString("access-token", "");
    }

    private String getClient() {
        return sharedPreferences.getString("client", "");
    }

    private String getUid() {
        return sharedPreferences.getString("uid", "");
    }
}
