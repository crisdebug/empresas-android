package com.samuel.desafioioasys.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.imageview.ShapeableImageView;
import com.google.android.material.textview.MaterialTextView;
import com.samuel.desafioioasys.R;
import com.samuel.desafioioasys.api.RetroFitProvider;
import com.samuel.desafioioasys.model.Empresa;
import com.samuel.desafioioasys.model.EmpresaResponse;

public class EmpresasAdapter extends RecyclerView.Adapter<EmpresasAdapter.EmpresaViewHolder> {

    private EmpresaResponse empresaResponse;

    public EmpresaResponse getEmpresaResponse() {
        return empresaResponse;
    }

    public void setEmpresaResponse(EmpresaResponse empresaResponse) {
        this.empresaResponse = empresaResponse;
    }

    private Context context;

    public EmpresasAdapter(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public EmpresaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        MaterialCardView cardView = (MaterialCardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.empresa_pesquisa_card, parent, false);
        EmpresaViewHolder viewHolder = new EmpresaViewHolder(cardView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull EmpresaViewHolder holder, int position) {
        Empresa empresa = empresaResponse.getEmpresas().get(position);
        Glide.with(context).load(RetroFitProvider.getImageUrl(empresa.getPhotoLink())).into(holder.empresaImagem);
        holder.empresaNome.setText(empresa.getName());
        holder.empresaTipo.setText(empresa.getEmpresaTipo().getTypeName());
        holder.empresaPais.setText(empresa.getCountry());

    }

    @Override
    public int getItemCount() {
        return empresaResponse == null ? 0 : empresaResponse.getEmpresas().size();
    }

    public class EmpresaViewHolder extends RecyclerView.ViewHolder {
        public MaterialCardView cardView;
        public ShapeableImageView empresaImagem;
        public MaterialTextView empresaNome;
        public MaterialTextView empresaTipo;
        public MaterialTextView empresaPais;


        public EmpresaViewHolder(MaterialCardView cardView) {
            super(cardView);
            this.empresaImagem = cardView.findViewById(R.id.empresa_image_view);
            this.empresaNome = cardView.findViewById(R.id.empresa_nome);
            this.empresaTipo = cardView.findViewById(R.id.empresa_tipo_nome);
            this.empresaPais = cardView.findViewById(R.id.empresa_pais_nome);
        }


    }
}
