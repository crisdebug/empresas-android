package com.samuel.desafioioasys.model;

import com.google.gson.annotations.SerializedName;

public class EmpresaTipo {
    @SerializedName("id")
    private long id;

    @SerializedName("enterprise_type_name")
    private String typeName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
