package com.samuel.desafioioasys.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class EmpresaResponse {
    @SerializedName("enterprises")
    private List<Empresa> empresas;

    public List<Empresa> getEmpresas() {
        return empresas;
    }

    public void setEmpresas(List<Empresa> empresas) {
        this.empresas = empresas;
    }
}
