package com.samuel.desafioioasys.model;

import com.google.gson.annotations.SerializedName;

public class Empresa {

    @SerializedName("id")
    private long id;

    @SerializedName("email_enterprise")
    private String email;

    @SerializedName("facebook")
    private String facebookLink;

    @SerializedName("twitter")
    private String twitterLink;

    @SerializedName("linkedin")
    private String linkedinLink;

    @SerializedName("phone")
    private String phone;

    @SerializedName("own_enterprise")
    private boolean isOwner;

    @SerializedName("enterprise_name")
    private String name;

    @SerializedName("photo")
    private String photoLink;

    @SerializedName("description")
    private String description;

    @SerializedName("city")
    private String city;

    @SerializedName("country")
    private String country;

    @SerializedName("value")
    private double value;

    @SerializedName("share_price")
    private double sharePrice;

    @SerializedName("enterprise_type")
    private EmpresaTipo empresaTipo;

    public EmpresaTipo getEmpresaTipo() {
        return empresaTipo;
    }

    public void setEmpresaTipo(EmpresaTipo empresaTipo) {
        this.empresaTipo = empresaTipo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getLinkedinLink() {
        return linkedinLink;
    }

    public void setLinkedinLink(String linkedinLink) {
        this.linkedinLink = linkedinLink;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isOwner() {
        return isOwner;
    }

    public void setOwner(boolean owner) {
        isOwner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoLink() {
        return photoLink;
    }

    public void setPhotoLink(String photoLink) {
        this.photoLink = photoLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getSharePrice() {
        return sharePrice;
    }

    public void setSharePrice(double sharePrice) {
        this.sharePrice = sharePrice;
    }
}
