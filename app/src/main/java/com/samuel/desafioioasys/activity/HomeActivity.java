package com.samuel.desafioioasys.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.MenuItemCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.textview.MaterialTextView;
import com.samuel.desafioioasys.R;
import com.samuel.desafioioasys.adapter.EmpresasAdapter;
import com.samuel.desafioioasys.api.IoasysRepository;
import com.samuel.desafioioasys.model.EmpresaResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private MaterialToolbar toolbar;
    private MaterialTextView searchTooltip;
    private MaterialTextView notFoundTooltip;
    private RecyclerView listEmpresasRecyclerView;
    private EmpresasAdapter adapter;
    private IoasysRepository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getToolbar();
        searchTooltip = findViewById(R.id.home_tooltip);
        notFoundTooltip = findViewById(R.id.not_found_tooltip);
        repository = new IoasysRepository(this);
        listEmpresasRecyclerView = findViewById(R.id.list_empresas_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        listEmpresasRecyclerView.setLayoutManager(layoutManager);
        adapter = new EmpresasAdapter(this);
        listEmpresasRecyclerView.setAdapter(adapter);
    }

    private MaterialToolbar getToolbar() {
        if (toolbar == null) {
            toolbar = findViewById(R.id.app_bar_home);
            if (toolbar != null) {
                setSupportActionBar(toolbar);
            }
        }
        return toolbar;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_home_menu, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        MenuItem searchItem = (MenuItem) menu.findItem(R.id.home_search_button);
        android.widget.SearchView searchView = (android.widget.SearchView) menu.findItem(R.id.home_search_button).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                searchTooltip.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                searchTooltip.setVisibility(View.VISIBLE);
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                notFoundTooltip.setVisibility(View.GONE);
                search(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                notFoundTooltip.setVisibility(View.GONE);
                search(newText);
                return true;
            }
        });

        return true;
    }

    public void search(String query) {
        if (query.isEmpty()) {
            adapter.setEmpresaResponse(null);
            adapter.notifyDataSetChanged();
        } else {
            Call<EmpresaResponse> empresasCall = this.repository.getEmpresasCall(query);
            empresasCall.enqueue(new Callback<EmpresaResponse>() {
                @Override
                public void onResponse(Call<EmpresaResponse> call, Response<EmpresaResponse> response) {
                    if (response.isSuccessful()) {
                        if(response.body().getEmpresas().size() == 0) {
                            notFoundTooltip.setVisibility(View.VISIBLE);
                        } else {
                            adapter.setEmpresaResponse(response.body());
                            adapter.notifyDataSetChanged();
                        }
                    }
                }

                @Override
                public void onFailure(Call<EmpresaResponse> call, Throwable t) {

                }
            });
        }

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}