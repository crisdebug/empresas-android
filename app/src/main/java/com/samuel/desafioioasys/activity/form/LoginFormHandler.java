package com.samuel.desafioioasys.activity.form;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.samuel.desafioioasys.R;
import com.samuel.desafioioasys.activity.MainActivity;
import com.samuel.desafioioasys.model.Credentials;

public class LoginFormHandler {
    private final AppCompatActivity activity;

    private TextInputLayout emailInputLayout;
    private TextInputEditText emailEditText;

    private TextInputLayout senhaInputLayout;
    private TextInputEditText senhaEditText;
    public MaterialButton loginButton;
    public MaterialTextView errorText;



    public LoginFormHandler(AppCompatActivity activity) {
        this.activity = activity;
        this.emailInputLayout = activity.findViewById(R.id.login_email_input_layout);
        this.emailEditText = activity.findViewById(R.id.login_email_text_edit);
        this.senhaInputLayout = activity.findViewById(R.id.login_senha_text_input_layout);
        this.senhaEditText = activity.findViewById(R.id.login_senha_text_edit);
        this.loginButton = activity.findViewById(R.id.login_button);
        this.errorText = activity.findViewById(R.id.login_error_text);
    }

    public boolean validate() {
        if (emailEditText.getText().toString().isEmpty()) return false;
        if (!Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches()) return false;
        if (senhaEditText.getText().toString().isEmpty()) return false;
        return true;
    }

    public Credentials toCredentials() {
        return new Credentials(emailEditText.getText().toString(), senhaEditText.getText().toString());
    }

    public void invalidate() {
//        emailInputLayout.invalidate();
//        senhaInputLayout.invalidate();
        emailInputLayout.setErrorIconDrawable(R.drawable.error);
        emailInputLayout.setErrorEnabled(true);
//        emailInputLayout.requestLayout();
        System.out.println(emailInputLayout.isErrorEnabled());
        senhaInputLayout.setErrorIconDrawable(R.drawable.error);
        senhaInputLayout.setErrorEnabled(true);
        senhaInputLayout.requestLayout();
        errorText.setVisibility(View.VISIBLE);
        loginButton.setEnabled(false);
        TextWatcher emailWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!loginButton.isEnabled()) loginButton.setEnabled(true);
                if (errorText.getVisibility() == View.VISIBLE) errorText.setVisibility(View.GONE);
                if (emailInputLayout.isErrorEnabled()) {
                    emailInputLayout.setError(null);
                    emailInputLayout.setErrorEnabled(false);
                    System.out.println(emailInputLayout.isErrorEnabled());
                }
                if (senhaInputLayout.isErrorEnabled()) {
                    senhaInputLayout.setError(null);
                    senhaInputLayout.setErrorEnabled(false);
                }

            }
        };

        emailEditText.addTextChangedListener(emailWatcher);

        TextWatcher senhaWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!loginButton.isEnabled()) loginButton.setEnabled(true);
                if (errorText.getVisibility() == View.VISIBLE) errorText.setVisibility(View.GONE);
                if (emailInputLayout.isErrorEnabled()) emailInputLayout.setErrorEnabled(false);
                if (senhaInputLayout.isErrorEnabled()) senhaInputLayout.setErrorEnabled(false);

            }
        };

        senhaEditText.addTextChangedListener(senhaWatcher);
    }


}
