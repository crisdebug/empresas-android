package com.samuel.desafioioasys.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.android.material.textview.MaterialTextView;
import com.samuel.desafioioasys.R;
import com.samuel.desafioioasys.activity.form.LoginFormHandler;
import com.samuel.desafioioasys.api.IoasysRepository;
import com.samuel.desafioioasys.model.Credentials;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private LoginFormHandler formHandler;
    private IoasysRepository repository;
    private View loadingOverlay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        formHandler = new LoginFormHandler(this);
        repository = new IoasysRepository(this);
        loadingOverlay = findViewById(R.id.progress_overlay);
        formHandler.loginButton.setOnClickListener(v -> onClickLoginButton());
    }

    private void onClickLoginButton() {
        if (formHandler.validate()) {
                Credentials credentials = formHandler.toCredentials();
                System.out.println("Aqui");
                login(credentials);
        }
    }

    private void goHome() {
        Intent a = new Intent(this, HomeActivity.class);
        startActivity(a);
        finish();
    }

    private void login(Credentials credentials) {
        Call<Void> signInCall = repository.getSignInCall(credentials);
        loadingOverlay.setVisibility(View.VISIBLE);
        signInCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    String uid = response.headers().get("uid");
                    String client = response.headers().get("client");
                    String accessToken = response.headers().get("access-token");
                    repository.saveHeaders(accessToken, client, uid);
                    goHome();
                } else {
                    loadingOverlay.setVisibility(View.GONE);
                    formHandler.invalidate();
                }

            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
}